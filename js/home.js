/*
personal_website is a website meant to show informations about someone.
Copyright (C) 2020  Félix Bertoni

this file is part of personal_website

personal_website is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

personal_website is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with personal_website.  If not, see <https://www.gnu.org/licenses/>.
*/

function gameInit(){
    createGame("gamecanvas");

    
    GAME.removeSPRect = homeCenterRect;

    var maincolor = getComputedStyle(document.documentElement).getPropertyValue('--main-color');
    var secondary = "#fffffff";

    GAME.playerColor = maincolor;
    GAME.spColor = secondary;

    //#FIXME clearing elements behind title of the game using root style
    GAME.load();
    startGame();
}


function homeCenterRect(){
    //#FIXME setting proper colors of the game using root style
    var homecenter = document.getElementById("home-center");
    var margin = 20;
    var cpos = new Vec(homecenter.offsetLeft - margin, GAME.renderer.height -  (homecenter.offsetTop + homecenter.clientHeight) - margin);
    var end = new Vec(homecenter.offsetLeft + homecenter.clientWidth + margin, GAME.renderer.height -  homecenter.offsetTop + margin);
   
    return new Rect(cpos, end);
}


function gameStop(){
    stopGame();
}

{
    gameInit();
}