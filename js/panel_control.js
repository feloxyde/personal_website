/*
personal_website is a website meant to show informations about someone.
Copyright (C) 2020  Félix Bertoni

this file is part of personal_website

personal_website is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

personal_website is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with personal_website.  If not, see <https://www.gnu.org/licenses/>.
*/

function selectPanel(target){
    /*setting right button with right color */
    var navButtons = document.getElementsByClassName("main-nav-btn");
    for( element of navButtons ){
        if(element.getAttribute("data-target") == target){
            element.classList.add("selected");
        } else {
            element.classList.remove("selected");
        }
    }

    /* setting visibility of panel */
    var panels = document.getElementsByClassName("panel");
    for( panel of panels){
        if(panel.id == target){
            panel.classList.remove("panel-hide");
        } else {
            panel.classList.add("panel-hide");
        }
    }

    if(target=="home") {
        document.getElementById("footer-placeholder").classList.add("footer-hide");
        document.getElementById("footer").classList.add("footer-hide");
        document.getElementById("main-nav-holder").classList.add("nav-hide");
        resumeGame();
        
    } else {
        document.getElementById("footer-placeholder").classList.remove("footer-hide");
        document.getElementById("footer").classList.remove("footer-hide");
        document.getElementById("main-nav-holder").classList.remove("nav-hide");
        stopGame();
    }

    updateURIFromState();

    mainNavHide();
}


function getSelectedPanel()
{
    for(element of document.getElementsByClassName("panel")){
        if(!element.classList.contains("panel-hide")){
            return element;
        }
    }
}

function mainNavShow(){
    var navtoggle = document.getElementById("main-nav-toggle");
    navtoggle.classList.remove("main-nav-toggle-expand");
    document.getElementById("main-nav").classList.add("main-nav-expand");
    navtoggle.innerHTML = "&#215;"
}

function mainNavHide(){
    var navtoggle = document.getElementById("main-nav-toggle");
    navtoggle.classList.add("main-nav-toggle-expand");
    document.getElementById("main-nav").classList.remove("main-nav-expand");
    navtoggle.innerHTML = "+";
}

{
    var navButtons = document.getElementsByClassName("main-nav-btn");
    for( element of navButtons ){
        element.addEventListener("click", function(){
            var target = this.getAttribute("data-target");
            selectPanel(target);
        });
    }

    
    document.getElementById("main-nav-toggle").addEventListener("click", function(){
        var navtoggle = document.getElementById("main-nav-toggle");
        if(navtoggle.classList.contains("main-nav-toggle-expand")){
            mainNavShow();
        } else {
            mainNavHide();
        }
    });
}

{
    var homeButtons = document.getElementsByClassName("home-button");
    for( element of homeButtons ){
        element.addEventListener("click", function(){
            var target = this.getAttribute("data-target");
            selectPanel(target);
        });
    }
}
