/*
personal_website is a website meant to show informations about someone.
Copyright (C) 2020  Félix Bertoni

this file is part of personal_website

personal_website is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

personal_website is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with personal_website.  If not, see <https://www.gnu.org/licenses/>.
*/

var courses = {};



function checkSearched (element, filterStrings, searchTitle, searchId, searchDescription){
    if(searchTitle == false && searchId ==false && searchDescription == false)
    {
        searchTitle = true;
        searchDescription = true;
        searchId = true;
    }

    var found = false;
    if(filterStrings.length > 1 || filterStrings[0] != ""){
    for (var i = 0; i < filterStrings.length && !found; ++i) {
        found = found || searchTitle && element.getElementsByClassName("course-name")[0]
                                        .textContent
                                        .toLowerCase()
                                        .includes(filterStrings[i]);
  
          // check in description
          found = found || searchDescription && element.getElementsByClassName("course-description")[0]
                                        .textContent
                                        .toLowerCase()
                                        .includes(filterStrings[i]);
          // check in id
          found = found || searchId && element.getElementsByClassName("course-code")[0]
                                        .textContent
                                        .toLowerCase()
                                        .includes(filterStrings[i]);
    }
    } else {
        found = true;
    }
    return found;
}


function checkClasses(element, tagsclasses, typeclasses, showWorkInProgress)
{
    var show = false;
    var show = showWorkInProgress && element.classList.contains("course-work-in-progress") || !element.classList.contains("course-work-in-progress");
    if(show){
        var showTags = false;
        if(tagsclasses.length > 0){
            for(const tagclass of tagsclasses){
                showTags = showTags || element.classList.contains(tagclass);
            }
        }
        else {
            showTags = true;
        }
        show = show && showTags;
    }

    if(show){
        var showType = false;
        if(typeclasses.length > 0){
            for(const typeclass of typeclasses){
                       showType = showType || element.classList.contains(typeclass);
            }
        }else {
            showType = true;
         }
        show = show && showType;
    }
    
     return show;
}

function searchCourseString()
{
    return document.getElementById("courses-filter-string").value.toLowerCase();
}

function showWorkInProgressChecked()
{
    return document.getElementById("filter-show-work-in-progress").checked;
}

function searchTitleChecked()
{
    return document.getElementById("filter-search-title").checked;
}

function searchIdChecked()
{
    return document.getElementById("filter-search-id").checked;
}

function searchDescriptionChecked()
{
    return document.getElementById("filter-search-description").checked;
}

function setSearchValue(value){
    document.getElementById("courses-filter-string").value = value;
}

function setShowWorkInProgress()
{
    return document.getElementById("filter-show-work-in-progress").checked = true;
}

function setSearchTitle()
{
    return document.getElementById("filter-search-title").checked = true;
}

function setSearchId()
{
    return document.getElementById("filter-search-id").checked = true;
}

function setSearchDescription()
{
    return document.getElementById("filter-search-description").checked = true;
}


function filter(){
    var showWorkInProgress = showWorkInProgressChecked();
    var searchTitle = searchTitleChecked();
    var searchId = searchIdChecked();
    var searchDescription = searchDescriptionChecked();

    var tagsclasses = [];
    for (checkbox of document.getElementsByClassName("filter-checkbox-tags")){
        if (checkbox.checked) {
            tagsclasses.push("course-tag-"+checkbox.id.substr(11));
        }
    }

    var typeclasses = [];
    for (checkbox of document.getElementsByClassName("filter-checkbox-types")){
        
        if (checkbox.checked) {
            typeclasses.push("course-type-"+checkbox.id.substr(12));
        }
    }

    var filterStrings = searchCourseString().split(";");
 

    for(element of document.getElementsByClassName("course-item")){
        if(checkSearched(element, filterStrings, searchTitle, searchId, searchDescription)
        && checkClasses(element, tagsclasses, typeclasses, showWorkInProgress))
        {
            element.classList.remove("course-hide");
        } else {
            element.classList.add("course-hide");
        }
    }
    updateURIFromState();
}


function buildFilter(filterid, filtername, type)
{
    var checkbox = document.createElement("label");
    checkbox.classList.add("custom-checkbox");
    checkbox.textContent = filtername;
    var input = document.createElement("input");
    input.classList.add("filter-checkbox");
    input.classList.add("filter-checkbox-"+type);
    input.id = filterid;
    input.type = "checkbox";
    checkbox.append(input);
    var span = document.createElement("span");
    span.classList.add("checkbox-checkmark");
    checkbox.append(span);
    input.addEventListener("change", function() {filter();});
    return checkbox;
}


function filterIdFromTag(tag){
    return "filter-tag-"+tag.split(" ").join("-");
}

function filterIdFromType(type){
    return "filter-type-"+type.split(" ").join("-");
}

function updateTagsFilters(newtags){
    for (const tag of newtags){
        //"lets keep elements in order !"
        if(document.getElementById(filterIdFromTag(tag)) === null){
            //create tagfilter here 

            var filter = buildFilter(filterIdFromTag(tag), tag, "tags");

            var element = null;
            //looking for right element, insert in code order
            for (var elm of document.querySelectorAll("#filter-group-tags .custom-checkbox"))
            {
                if(element === null && elm.textContent > filter.textContent ){
                    element = elm;
                }

            }
            if(element !== null){
                element.before(filter);
            } else {
                document.getElementById("filter-group-tags").append(filter);
            }
        }
    }
}

function updateTypesFilters(newtypes){
    for (const type of newtypes){
        //"let's keep elements in order"
        if(document.getElementById(filterIdFromType(type)) === null){


            //create tagfilter here 
            
            var filter = buildFilter(filterIdFromType(type), type, "types");

            var element = null;
            //looking for right element, insert in code order
            for (var elm of document.querySelectorAll("#filter-group-types .custom-checkbox"))
            {
                if(element === null && elm.textContent > filter.textContent ){
                    element = elm;
                }

            }
            if(element !== null){
                element.before(filter);
            } else {
                document.getElementById("filter-group-types").append(filter);
            }
        }
    }
}


class Course {
    constructor(name, code, path){
        this.name = name;
        this.file = COURSE_MAIN_FILENAME;
        this.identifier = path.split("/").pop().split("_").join("-");
        this.code = code;
        this.description = "";
        this.path = path;
        this.tags = [];
        this.forms = [];
        this.workInProgress = true;
    }

    url(){
        return GITLAB_ADDRESS + GITLAB_COURSES_REPOS + GITLAB_FILE_ACCESS_SUFFIX + this.path + "/" + this.file;
    }

    buildHeader(){
        var courseHeader = document.createElement("div");
        courseHeader.classList.add("course-header");
            var title = document.createElement("h3");
            title.classList.add("course-name");
            title.textContent = this.name;
            courseHeader.append(title);
            
            
        return courseHeader;
    }

    buildDetails(){
        var courseDetails = document.createElement("div");
        courseDetails.classList.add("course-details");
            var detailsLine = document.createElement("div");
            detailsLine.classList.add("course-details-line");

            var code = document.createElement("div");
            code.classList.add("course-code");
            code.textContent =  this.code;
            detailsLine.append(code);

            var courseType = document.createElement("div");
            courseType.classList.add("course-types");
            courseType.textContent = this.forms.join(", ");
            detailsLine.append(courseType);

            var courseTags = document.createElement("div");
            courseTags.classList.add("course-tags");
            courseTags.textContent = this.tags.join(", ");

            detailsLine.append(courseTags);

            courseDetails.append(detailsLine);
            var courseDescription = document.createElement("div");
            courseDescription.classList.add("course-description");
            courseDescription.textContent = this.description;
            courseDetails.append(courseDescription);
           
        return courseDetails;
    }

    buildControls()
    {
        var courseControls = document.createElement("div");
        courseControls.classList.add("course-controls"); 
       /* var openCourseButton = document.createElement("a");
        openCourseButton.classList.add("course-view");
        openCourseButton.href = "contentviewer.html?"+encodeURIComponent(this.path);
        openCourseButton.textContent = "view";
        courseControls.append(openCourseButton); */
        var gitlabButton = document.createElement("a");
        gitlabButton.classList.add("course-link");
        gitlabButton.href = this.url();
        gitlabButton.textContent = "see on gitlab";
        /* #FIXME add event listener here */
        courseControls.append(gitlabButton);
       
        return courseControls;
    }

    insertInDOM()
    {
        var course = document.createElement("div");
        course.classList.add("course-item");
        course.id="course-id-" + this.identifier;
        for(const type of this.forms){
            course.classList.add("course-type-"+type.split(" ").join("-"));
        }

        updateTypesFilters(this.forms);

        for(const tag of this.tags){
            course.classList.add("course-tag-"+tag.split(" ").join("-"));
        }

        updateTagsFilters(this.tags);

        if(this.workInProgress)
        {
            course.classList.add("course-work-in-progress");
        }
        
        course.append(this.buildHeader());
        var courseBlock = document.createElement("div");
        courseBlock.classList.add("course-block");
        courseBlock.append(this.buildDetails());
        courseBlock.append(this.buildControls());
        course.append(courseBlock);

        var element = null;
        //looking for right element, insert in code order
        for (var elm of document.getElementsByClassName("course-item"))
        {

            if(element === null && elm.getElementsByClassName("course-code")[0].innerHTML > this.code ){
                element = elm;
            }
        }
        if(element !== null){
            element.before(course);
        } else {
            document.getElementById("courses-list").append(course);
        }

    }

    load()
    {
        var rrequest = new XMLHttpRequest();
         rrequest.open('GET', GITLAB_API_ADDRESS + encodeURIComponent(GITLAB_COURSES_REPOS)+'/repository/files/'+encodeURIComponent(this.path + COURSE_META_FILENAME) + "/raw?ref=master", true);
        var courseid = this.identifier;
        rrequest.onload = function () {
        // Begin accessing JSON data here
            var meta = JSON.parse(this.response);
            var course = courses[courseid];
            if(meta.description !== undefined){
                course.description = meta.description;
            } 
            if(meta.tags !== undefined)
            {
                course.tags = meta.tags;
            }
            if(meta.forms !== undefined)
            {
                course.forms = meta.forms;
            }
            if(meta.workInProgress !== undefined)
            {
                course.workInProgress = meta.workInProgress;
            }
            if(meta.name !== undefined)
            {
                course.name = meta.name;
            }
          
            course.insertInDOM();

        }

        rrequest.onerror = function(){
            console.log("something went wrong");
        }
        rrequest.send();
    }


}

function createCourseFromTreeElement(element){
    var nameParts = element.name.split("_");
    var code = nameParts.shift();
    var name = nameParts.join(" ");
    var path = element.path;
    return new Course(name, code, path);
}

function parseGitlabTree(elements)
{
    elements.forEach(element => {
        if(element.type === "tree")
        {
            var path = element.path.split("/");
            if(path.length == 1) {
               var course = createCourseFromTreeElement(element);
               courses[course.identifier] = course;
               course.load();
            }
        }
    });
}

function pullCourses(repos)
{
    var request = new XMLHttpRequest();
    request.open('GET', GITLAB_API_ADDRESS + encodeURIComponent(GITLAB_COURSES_REPOS)+'/repository/tree?recursive=true', true);
    request.onload = function () {
        // Begin accessing JSON data here
        var data = JSON.parse(this.response);
        parseGitlabTree(data);
        
    }

    request.onerror = function(){
        console.log("something went wrong !");
    }
    request.send();

}


function previewCourse(coursepath){
    //get file.

    //render file as markdown

}


{
    pullCourses(GITLAB_COURSES_REPOS);
}



{
    //initializing 
    document.getElementById("courses-filter-string").addEventListener("input", function(){
        filter();
    });

    document.getElementById("filter-show-work-in-progress").addEventListener("change", function(){
        filter();
    });

    document.getElementById("filter-search-title").addEventListener("change", function(){
        filter();
    });

    document.getElementById("filter-search-id").addEventListener("change", function(){
        filter();
    });

    document.getElementById("filter-search-description").addEventListener("change", function(){
        filter();
    });

    document.getElementById("courses-expand-filters").addEventListener("click", function(){
        var options = document.getElementById("courses-filter-options");
        if(options.classList.contains("filter-options-hide")){
            options.classList.remove("filter-options-hide");
            document.getElementById("courses-expand-filters").innerHTML="hide filters";
        } else {
            options.classList.add("filter-options-hide");
            document.getElementById("courses-expand-filters").innerHTML="show filters";
        }
    });

}