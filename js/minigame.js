/*
personal_website is a website meant to show informations about someone.
Copyright (C) 2020  Félix Bertoni

this file is part of personal_website

personal_website is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

personal_website is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with personal_website.  If not, see <https://www.gnu.org/licenses/>.
*/


class Vec{
    constructor(x, y){
        this.x = x;
        this.y = y;
    }

    cap(threshold) {
        if(-threshold <= this.x && threshold >= this.x){
            this.x = 0;
        }
        if(-threshold <= this.y && threshold >= this.y){
            this.y = 0;
        }

    }

    add(vec){
        return new Vec(this.x + vec.x,  this.y + vec.y);
    }

    multiply(float){
        return new Vec(this.x * float, this.y * float);
    }
}

class Renderer {
    constructor(canvasId){
        this.canvas = document.getElementById(canvasId);
        if(this.canvas.getContext){
        this.context = this.canvas.getContext("2d");
        this.valid = true;
        } else {
            this.context = null;
            this.valid = false;
        }
        this.width = this.canvas.scrollWidth;
        this.height = this.canvas.scrollHeight;
        this.canvas.height = this.height;
        this.canvas.width = this.width;
    }

    newFrame(){
        //erase entire canvas at once and update width/height if needed
        
       /* this.width = this.canvas.scrollWidth;
        this.height = this.canvas.scrollHeight;
        this.canvas.height = this.height;
        this.canvas.width = this.width; */
        this.context.clearRect(0, 0, this.width, this.height);
    }

    drawSquare(pos, size, color){
        this.context.fillStyle = color;
        this.context.fillRect(pos.x, this.height - pos.y - size, size, size);
    }

    drawText(text, pos, size, color){
        this.context.font = size+" monospace";
        this.context.fillStyle = color; 
        this.context.fillText(text, pos.x, pos.y);
    }

    drawEmptySquare(pos, size, color){
        var lineWidth = 5;
        this.context.fillStyle = color;
        this.context.fillRect(pos.x, this.height- pos.y - size, size, size);
        this.context.clearRect(pos.x + lineWidth, this.height - (pos.y + lineWidth) - (size - lineWidth*2), size - lineWidth*2, size - lineWidth*2);
    }
}

class Rect {
    constructor(begin, end){
        this.begin = begin;
        this.end = end;
    }

    translate(vec){
        this.begin = this.begin.add(vec);
        this.end = this.end.add(vec);
    }

    overlap(rect){
        return !( this.end.x < rect.begin.x 
            || this.end.y < rect.begin.y
            || this.begin.x > rect.end.x
            || this.begin.y > rect.end.y);
    }

}

class Player {
    constructor(size, color)
    {
        this.body = new Rect(new Vec(0, 0), new Vec(size, size));
        this.size = size;
        this.bounce = 0.8;
        this.speed = new Vec(0, 0);
        this.color = color;

        this.impulseReady = true;
        this.gravity = -50;
        this.speed = new Vec(0,0);
        this.threshold = 0.1;
        this.friction = 0.1;
    }

    center(){
        return new Vec(this.body.begin.x + this.size/2, this.body.begin.y + this.size/2);
    }

    impulse(vec){
        if(this.impulseReady == true){
            this.speed = this.speed.add(vec);
            this.impulseReady = false;
        }
    }

    wallCollisions(ir){
        if(this.body.begin.x <= 0){
            this.speed.x = -this.speed.x * this.bounce;
            this.body.translate(new Vec(-this.body.begin.x, 0));
            this.impulseReady = true;
        } 
        if(this.body.begin.y <= 0){
            this.speed.y = -this.speed.y * this.bounce;
            this.body.translate(new Vec(0, -this.body.begin.y));
            this.impulseReady = true;
            //if there is no bounce, apply friction
            if (-this.threshold*10 <= this.speed.y && this.threshold*10 >= this.speed.y){
                if(this.speed.x != 0){
                var friction = this.friction * ir * (this.speed.x/Math.abs(this.speed.x));
                if(Math.abs(friction) >= Math.abs(this.speed.x)){
                    this.speed.x = 0;
                }
                else {
                    this.speed.x -= friction;
                }
                }
            }
            GAME.reset();
        } 
        if(this.body.end.x >= GAME.renderer.width){
            this.speed.x = -this.speed.x * this.bounce;
            this.body.translate(new Vec(GAME.renderer.width - this.body.end.x, 0));
            this.impulseReady = true;

        }
        if(this.body.end.y >= GAME.renderer.height){
            this.speed.y = -this.speed.y * this.bounce;
            this.body.translate(new Vec(0 , GAME.renderer.height - this.body.end.y));
            this.impulseReady = true;
        } 
    }

    update(interval)
    {

        //gravity
        var ir = interval/1000;

        this.speed.y += this.gravity * ir;

        //checking for collisions with walls
        this.body.translate(this.speed.multiply(ir));
        this.wallCollisions(ir);
        
        //cap speed
        this.speed.cap(this.threshold);
    }
}


class SquarePoint {
    constructor(size, color)
    {
        this.body = new Rect(new Vec(0, 0), new Vec(size, size));
        this.size = size;
        this.color = color;
        this.active = true;
    }
}

var mouseX = 0;
var mouseY = 0;
var mouseXPrev = 0;
var mouseYPrev = 0;
var mouseExited = false;
var impulseX = 0;
var impulseY = 0;
var impulseSet = false;

function mouseExitTracking(event){
    mouseX = event.clientX;
    mouseY = event.clientY;
    mouseExited = true;
}

function mouseDownTracking(event){
    mouseExited = false;
    mouseXPrev = event.clientX;
    mouseYPrev = event.clientY;
}

function mouseUpTracking(event){
    if(!mouseExited){
        mouseX = event.clientX;
        mouseY = event.clientY;
    }

    impulseX = mouseX - mouseXPrev;
    impulseY = mouseY - mouseYPrev;
    impulseSet = true;
}

function getImpulse()
{
    var impulse = new Vec(impulseX, -impulseY);
    impulseSet = false;
    impulseX = 0;
    impulseY = 0;
    return impulse;
}
 
function isImpulseSet(){
    return impulseSet;
}


class Game{
    constructor(canvasId)
    {
        this.canvasId = canvasId;
        this.score = 0;
        this.maxScore = 0;
        this.playerColor = "#0000ff";
        this.spColor = "#00ffff";
        this.textColor = "#ffffff";
        this.removeSPRect = function(){
            return new Rect(new Vec(0,0), new Vec(0,0))
        };
    }
    
    load(){
        this.renderer = new Renderer(this.canvasId);

        this.player = new Player(50, this.playerColor);
        this.player.body.translate(new Vec(100,this.renderer.height/3));
        this.player.gravity = -(this.renderer.height / 2);
        this.player.bounce = 0;
        this.player.friction = 100;
        this.player.threshold = this.renderer.height / 2000; 
        /* we want to set gravity to a percentage of canvas height, same for sizes ? */
        this.previousTime = 0;
        this.interval = 0;

        this.innerFrameBorder = 100;
        this.innerFrameBottomBorder = this.renderer.height/5;
        
        var minscreen = Math.min(this.renderer.width, this.renderer.height);

        this.spMinSize = minscreen/50;
        this.spMaxSize = minscreen/25;

        var scrRatio = this.renderer.width / this.renderer.height;

        this.psCountX = Math.floor(7*scrRatio);
        this.psCountY = Math.floor(7/scrRatio);



        this.scoreSize = Math.floor(this.renderer.height/40);

        this.sp = [];

        this.renderer.canvas.onmouseleave = mouseExitTracking;
        this.renderer.canvas.onmousedown = mouseDownTracking;

        this.generateSPs(this.psCountX, this.psCountY);
        this.deleteSPInRect(this.removeSPRect());
        this.score = 0;
        this.best = 0;
    }

    incrementScore(){
        this.score++;
        if(this.score > this.best){
            this.best = this.score;
        }
    }
    
    resetScore(){
        this.score = 0;
    }

    generateSPs(xdensity,ydensity){
        //split panel into divisions
        var minX = 0 + this.innerFrameBorder;
        var maxX = this.renderer.width - this.innerFrameBorder;
        var minY = 0 + this.innerFrameBottomBorder;
        var maxY = this.renderer.height - this.innerFrameBorder;

        var xAmplitude = maxX - minX;
        var yAmplitude = maxY - minY;

        var divX = xAmplitude/xdensity;
        var divY = yAmplitude/ydensity;

        var sizeAmplitude = this.spMaxSize - this.spMinSize;
        //then generate a set of random coordinates in each square, while ensuring square is 
        //still in the cube

        for(var x = 0; x < xdensity; x++){
            for(var y = 0; y < ydensity; y++){
                var xm = x*divX + minX;
                var ym = y*divY + minY;


                var size = Math.random()*sizeAmplitude + this.spMinSize;
                var pos =  new Vec(Math.random()*divX + xm, Math.random()*divY + ym);
                pos = pos.add(new Vec(-size/2, -size/2));
                
                var sp = new SquarePoint(size, this.spColor);
                sp.body.translate(pos);
                this.sp.push(sp);
            }
        }

    }

    deleteSPInRect(rect) {

        for (var i = 0; i < this.sp.length; ){
                if(rect.overlap(this.sp[i].body)){
                    this.sp.splice(i, 1);
                } else {
                    i++;
                }
        }
    }

    spPlayerCollisison(){
        for(const sp of this.sp){
            if(sp.body.overlap(this.player.body) && sp.active){
                this.player.impulseReady = true;
                sp.active = false;
                this.incrementScore();
            }
        }
    }

    reset(){
        this.resetScore();for(const sp of this.sp){
                sp.active = true;
        }
        
    }

    nextFrame(time)
    {
        
        if(this.renderer.width != this.renderer.canvas.scrollWidth || this.renderer.height != this.renderer.canvas.scrollHeight){
            this.load();
        }


        this.interval = time - this.previousTime;
        this.previousTime = time;
        
        this.spPlayerCollisison();

        //getting player input
        if(isImpulseSet()){
            var impulse = getImpulse();
            var impulse = impulse.multiply(3.0);
            //apply impulse 
            this.player.impulse(impulse);
        }

        this.player.update(this.interval);

        this.renderer.newFrame();

        //drawing all SP
        for (const sp of this.sp){
            if(sp.active){
                this.renderer.drawEmptySquare(sp.body.begin, sp.size, sp.color);
            }
        }

        //drawing player
        this.renderer.drawSquare(this.player.body.begin, this.player.size, this.player.color);

        //displaying score 
        this.renderer.drawText("score : " + this.score, new Vec(this.innerFrameBorder/2,this.innerFrameBorder/2), "bold " + this.scoreSize + "px",  this.textColor);
        this.renderer.drawText("best  : " + this.best, new Vec(this.innerFrameBorder/2, this.innerFrameBorder/2 + this.scoreSize), "bold " + this.scoreSize + "px",  this.textColor);
    
    }

    firstFrame(time){
        this.interval = 0;
        this.previousTime = time;
    }
}

function oneframe(time)
{
    if(GAMEstop == false) {
        GAME.nextFrame(time);
        window.requestAnimationFrame(oneframe);
    }
}

function resumeGame(){
    GAMEstop = false;
    window.requestAnimationFrame(oneframe); 
}

function firstframe(time)
{
  GAME.firstFrame(time);

  window.requestAnimationFrame(oneframe);
}

function startGame()
{
    window.requestAnimationFrame(firstframe);
}

var GAME = null;
var GAMEstop = false;

function createGame(gamecanvas){
    GAME = new Game("gamecanvas");
    document.onmouseup = mouseUpTracking;
}

function stopGame(){
    GAMEstop = true;
}

