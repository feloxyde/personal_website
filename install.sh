#!/bin/bash

if [ -z "$1" ] && [ -z "$2" ]
  then
    echo "Error, this script shall be called with ./script source destination"
    exit 1
fi

echo "installing $1 at $2"

#erasing destination
rm -rf $2/*

#copying
cp -R $1/* $2

#stripping useless parts away
rm -rf $2/.git $2/README.md $2/LICENCE.txt $2/install.sh