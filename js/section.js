/*
personal_website is a website meant to show informations about someone.
Copyright (C) 2020  Félix Bertoni

this file is part of personal_website

personal_website is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

personal_website is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with personal_website.  If not, see <https://www.gnu.org/licenses/>.
*/

function toggleSectionCollapse(target){
    if(target.classList.contains("section-collapse")){
        target.classList.remove("section-collapse");
    } else {
        target.classList.add("section-collapse");
    }
}

{
    var sectionTops = document.getElementsByClassName("section-top");
    for (sTop of sectionTops) {
        sTop.addEventListener("click", function(){
            var parent = this.parentElement;
            toggleSectionCollapse(parent);
        });
    }
}