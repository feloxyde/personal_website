/*
personal_website is a website meant to show informations about someone.
Copyright (C) 2020  Félix Bertoni

this file is part of personal_website

personal_website is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

personal_website is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with personal_website.  If not, see <https://www.gnu.org/licenses/>.
*/

function updateStateFromURI() {
    var uri = window.location.href.split("?");

    var argsarr = decodeURI(uri[1]).split("&");

    var args = {};

    for (const arg of argsarr) {
        var split = arg.split("=");
        args[split[0]] = split[1];
    }

    var panel = args["panel"] !== undefined ? args["panel"] : "home";

    selectPanel(panel);
    /*
        if (args["search"] !== undefined) {
            setSearchValue(decodeURIComponent(args["search"]));
        }

        if (args["intitle"] !== undefined) {
            setSearchTitle();
        }

        if (args["inid"] !== undefined) {
            setSearchId();
        }

        if (args["indescription"] !== undefined) {
            setSearchDescription();
        }

        if (args["showworkinprogress"] === undefined) {
            setShowWorkInProgress();
        }

        filter();
    */
}



function updateURIFromState() {
    var panel = getSelectedPanel();

    var argStr = "?panel=" + panel.id;
    /*
        if (!showWorkInProgressChecked()) {
            argStr += "&showworkinprogress=0";
        }

        var search = searchCourseString();

        if (search != "") {
            argStr += "&search=" + encodeURIComponent(search);
        }

        if (searchTitleChecked()) {
            argStr += "&intitle=1";
        }
        if (searchIdChecked()) {
            argStr += "&inid=1";
        }
        if (searchDescriptionChecked()) {
            argStr += "&indescription=1";
        }
    */
    window.history.replaceState(null, null, window.location.pathname + argStr);
}

{
    updateStateFromURI();
}