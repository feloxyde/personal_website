# personal_website


This my personal website. It's design is inspired -somehow- from MDN's website design. 
It is client-side only and pulls courses and talks list from another Gitlab repository. 

# read before reuse code

This website's code quite a mess and needs factorization. I will clear it when I have time for that. 
This website is under AGPL licence, which means you derive this website to use it for yourself, you have to publish sources of your version
of the site under same licence. Please see [LICENCE.md](LICENCE.md) for more details. Be careful to clear text fields correctly
if you use the site as-is. 

In case you plan to use this website, please notify me on gitlab or by mail at felix.bertoni987@gmail.com.

# Licencing 

This website is under AGPL licence. 
Everything inside ```resources/``` directory is not part of the software and is licenced under CC-BY-SA [https://creativecommons.org/licenses/by-sa/4.0/](https://creativecommons.org/licenses/by-sa/4.0/). 

