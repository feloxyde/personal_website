/*
personal_website is a website meant to show informations about someone.
Copyright (C) 2020  Félix Bertoni

this file is part of personal_website

personal_website is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

personal_website is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with personal_website.  If not, see <https://www.gnu.org/licenses/>.
*/

var GITLAB_COURSES_REPOS_ID = "18448764"
var GITLAB_COURSES_REPOS = "feloxyde/courses_ressources";
var GITLAB_API_ADDRESS = "https://gitlab.com/api/v4/projects/";
var GITLAB_ADDRESS = "https://gitlab.com/";
var GITLAB_FILE_ACCESS_SUFFIX = "/-/blob/master/";
var COURSE_META_FILENAME = "/meta.json";
var COURSE_MAIN_FILENAME = "main.md";
var GITLAB_MARKDOWN_API = "https://gitlab.com/api/v4/markdown";